/* Copyright (C) 2020 Charadon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

#define KEY_RETURN 10
//Colors
	#define PLAYER_PADDLE_COLOR 1
	#define BALL_BARRIER_COLOR 2
	#define ENEMY_PADDLE_COLOR 3
	#define PLAYER_SCORE_COLOR 4
	#define ENEMY_SCORE_COLOR 5

//Declare Functions
	void *player_paddle();
	void *enemy_paddle();
	void end_game();
	void *the_ball();
	void *game_graphics();
	void flip_ball();
	void title_screen();
	void score_calculator();
	void victory_for_player();
	void victory_for_enemy();
//Declare Variables
	bool GAME_ENDED = TRUE; //Determines if game is still going.
	bool BARRIER_RENDER = TRUE;
	int tick = 100000; //Internal Clock
	int player_key_press;
	int player_paddle_location[2] = {9, 0}; //For Location Variables: First number is Y axis, and second number is X axis.
	int enemy_paddle_location[2] = {10, 80};
	int the_ball_location[2] = {10, 40}; //First Number is Row(Y) and Second number is Column(X)
	int ball_direction = 0; // 0 is left, 1 is right.
	int ball_angle = 0; // -1 is down, 1 is up.
	int dice; //Randomizer
	int score[2] = {0, 0}; //First number is Player score, second number is Enemy Score.
	int menu_selection = 0; //What option in the menu that's selected.
	int difficulty = 0; //1,000,000 is easy, 500,000 is normal, 100,000 is hard, 50,000 is Impossible.
	int barrier_render = -1;

int main(){
	//Initialization
		srand(time(NULL)); //Generates seed for randomizer based on current time.
		initscr(); //Initializes ncurses
		raw(); //Does something or another. Related to key presses.
		keypad(stdscr, TRUE); //I think it enables stuff like arrow keys? I don't fucking know.
		noecho(); //Doesn't echo key presses.
		nodelay(stdscr, TRUE);
		start_color(); //Initializes Color
	//Creating Colors
		init_pair(1, COLOR_GREEN, COLOR_GREEN); //Player's Paddle
		init_pair(2, COLOR_WHITE, COLOR_BLACK); //The Ball & Barrier
		init_pair(3, COLOR_RED, COLOR_RED); //Enemy Paddle
		init_pair(4, COLOR_GREEN, COLOR_BLACK); //Player Score
		init_pair(5, COLOR_RED, COLOR_BLACK); //Enemy Score
	//Title Screen
		title_screen();
		nodelay(stdscr, FALSE);
	//Thread stuff
		pthread_t ball; //Declares name of thread
		pthread_t player_paddle_thread;
		pthread_t graphics;
		pthread_t opposing_paddle;
		pthread_create(&graphics, NULL, game_graphics, NULL); //Activates thread.
		pthread_create(&ball, NULL, the_ball, NULL);
		pthread_create(&player_paddle_thread, NULL, player_paddle, NULL);
		pthread_create(&opposing_paddle, NULL, enemy_paddle, NULL);
		pthread_join(graphics, NULL); //Waits for thread to complete before continuing. In this case though, it'll never come back and finish main()
}

void *player_paddle(){
	while (GAME_ENDED == FALSE){
		usleep(1000);
		player_key_press = wgetch(stdscr);
		switch(toupper(player_key_press)){ //Registers key presses by the player.
			case KEY_UP:
				player_paddle_location[0] -= 2;
				break;
			case KEY_DOWN:
				player_paddle_location[0] += 2;
				break;
			case 'Q':
				end_game();
				break;
			default:
				break;
		}
		if (player_paddle_location[0] <= -1){
			player_paddle_location[0] = 0;
		}
		else if (player_paddle_location[0] + 2 >= 20){
			player_paddle_location[0] = 18;
		}
	}
	return NULL;
}

void *game_graphics(){
	while(GAME_ENDED == FALSE){
		usleep(30000);
		score_calculator();
		wclear(stdscr);
		//Draw Player's Paddle
		wattron(stdscr, COLOR_PAIR(PLAYER_PADDLE_COLOR));
			mvwaddch(stdscr, player_paddle_location[0], player_paddle_location[1], '|');
			mvwaddch(stdscr, player_paddle_location[0] + 1, player_paddle_location[1], '|');
			mvwaddch(stdscr, player_paddle_location[0] + 2, player_paddle_location[1], '|');
		wattroff(stdscr, COLOR_PAIR(PLAYER_PADDLE_COLOR));
		//Draws Enemy Paddle
		wattron(stdscr, COLOR_PAIR(ENEMY_PADDLE_COLOR));
			mvwaddch(stdscr, enemy_paddle_location[0], enemy_paddle_location[1], '|');
			mvwaddch(stdscr, enemy_paddle_location[0] + 1, enemy_paddle_location[1], '|');
			mvwaddch(stdscr, enemy_paddle_location[0] + 2, enemy_paddle_location[1], '|');
		wattroff(stdscr, COLOR_PAIR(ENEMY_PADDLE_COLOR));
		//Barrier Renderer
		while (BARRIER_RENDER == TRUE){
			attron(COLOR_PAIR(BALL_BARRIER_COLOR));
			barrier_render += 1;
			mvaddch(barrier_render, 40, '|');
			attroff(COLOR_PAIR(BALL_BARRIER_COLOR));
			if (barrier_render == 20){
				barrier_render = -1;
				break;
			}
			
		}
		//Draws the ball
		wattron(stdscr, COLOR_PAIR(BALL_BARRIER_COLOR));
			mvwaddch(stdscr, the_ball_location[0], the_ball_location[1], 'o');
		wattroff(stdscr, COLOR_PAIR(BALL_BARRIER_COLOR));
		//Draws Player's Score
		wattron(stdscr, COLOR_PAIR(PLAYER_SCORE_COLOR));
			mvwprintw(stdscr, 21, 0, "Green: %i", score[0]);
		wattroff(stdscr, COLOR_PAIR(PLAYER_SCORE_COLOR));
		//Draws Enemy Score
		wattron(stdscr, COLOR_PAIR(ENEMY_SCORE_COLOR));
			mvwprintw(stdscr, 21, 74, "Red: %i", score[1]);
		wattroff(stdscr, COLOR_PAIR(ENEMY_SCORE_COLOR));
		/* //Draws Debug Stuff
		mvprintw(20, 0, "Ball Angle: %i", ball_angle);
		mvprintw(21, 0, "Dice: %i", dice);
		mvprintw(22, 0, "Key Press: %i", player_key_press);
		mvprintw(23, 0, "Player Score: %i", score[0]);
		mvprintw(24, 0, "Enemy Score: %i", score[1]);
		mvprintw(25, 0, "Tick Speed: %i", tick); */
		wrefresh(stdscr);
	}
	return NULL;
}

void *enemy_paddle(){
	while(GAME_ENDED == FALSE){
		usleep(difficulty);
		if(ball_direction == 1){
			if (enemy_paddle_location[0] > the_ball_location[0]){
				enemy_paddle_location[0]--; //Down
			}
			else if(enemy_paddle_location[0] < the_ball_location[0]){
				enemy_paddle_location[0]++; //Up
			}
		}
		if (enemy_paddle_location[0] <= -1){
			enemy_paddle_location[0] = 0;
		}
		else if (enemy_paddle_location[0] + 2 >= 20){
			enemy_paddle_location[0] = 18;
		}
	}
	return NULL;
}

void score_calculator(){
	if (score[0] == 5){
		victory_for_player();
		return;
	}
	else if(score[1] == 5){
		victory_for_enemy();
		return;
	}
	else{
		return;
	}
	return;
}

void *the_ball(){
	while(GAME_ENDED == FALSE){
		usleep(tick); //The internal "clock" which determines the speed of the ball.
		//Detects if the ball has hit the player's paddle.
		flip_ball();
		switch(ball_angle){
			case -1: //Down
				switch(ball_direction){
					case 0: //Left
						the_ball_location[1]--;
						the_ball_location[0]++;
						break;
					case 1: //Right
						the_ball_location[1]++;
						the_ball_location[0]++;
						break;
					default:
						break;
				}
				break;
			case 1: //Up
				switch(ball_direction){
					case 0: //Left
						the_ball_location[1]--;
						the_ball_location[0]--;
						break;
					case 1: //Right
						the_ball_location[1]++;
						the_ball_location[0]--;
						break;
					default:
						break;
				}
				break;
			default: //Forward
				switch(ball_direction){
					case 0: //Left
						the_ball_location[1]--;
						break;
					case 1: //Right
						the_ball_location[1]++;
						break;
					default:
						break;
				}
				break;
		}
	}
	return NULL;
}

void flip_ball(){
	//Player Paddle Collision Detection
	if (the_ball_location[0] >= player_paddle_location[0] && the_ball_location[0] <= player_paddle_location[0] + 2 && the_ball_location[1] == player_paddle_location[1] + 1){
			dice = rand() % 3; //Rolls a d2
			ball_direction = 1;
			switch(dice){ //Determines ball's angle
			case 0: //Down
				ball_angle = -1;
				break;
			case 1:
				ball_angle = -1;
				break;
			case 2: //Up
				ball_angle = 1;
				break;
			case 3:
				ball_angle = 1;
				break;
			}
			if (tick >= 20000){ //Everytime the ball bounces from one of the player, it decreases the tick speed, thus speeding up the ball. With a max of 10,000.
				tick -= 10000;
			}
	}
	//The enemy paddle collision detection.
	else if (the_ball_location[0] >= enemy_paddle_location[0] && the_ball_location[0] <= enemy_paddle_location[0] + 2 && the_ball_location[1] == enemy_paddle_location[1] - 1){
			dice = rand() % 3; //Rolls a d2
			ball_direction = 0;
			switch(dice){ //Determines ball's angle
			case 0: //Down
				ball_angle = -1;
				break;
			case 1:
				ball_angle = -1;
				break;
			case 2: //Up
				ball_angle = 1;
				break;
			case 3:
				ball_angle = 1;
				break;
			}
			if (tick >= 20000){ //Everytime the ball bounces from one of the player, it decreases the tick speed, thus speeding up the ball. With a max of 10,000.
				tick -= 10000;
			}
	}
	//Wall Collision
	if (the_ball_location[1] < 0){ //Player's side
		the_ball_location[0] = 10;
		the_ball_location[1] = 40;
		ball_direction = 0;
		ball_angle = 0;
		score[1] += 1; //Score 1 for Enemy
		tick = 100000; //Reset clock
	}
	else if (the_ball_location[1] > 80){ //Enemy's Side
		the_ball_location[0] = 10;
		the_ball_location[1] = 40;
		ball_direction = 1;
		ball_angle = 0;
		score[0] += 1; //Score 1 for player
		tick = 100000; //Reset clock
	}
	else if (the_ball_location[0] >= 20){ //Bottom wall
		ball_angle = abs(ball_angle); //Inverts integer of ball_angle.
	}
	else if(the_ball_location[0] <= 0){ //Top wall
		ball_angle = -abs(ball_angle); //Inverts integer of ball_angle.
	}
	return;
}

void title_screen(){
	resize_term(30, 81);
	while (GAME_ENDED == TRUE){
		usleep(100000);
		clear();
		//Draws title screen
		mvprintw(0, 0, "+==============================================================================+");
		mvprintw(1, 0, "|                OOOOO     OOOOO     O     O     OOOOO                         |");
		mvprintw(2, 0, "|                O   O     O   O     O O   O     O                             |");
		mvprintw(3, 0, "|                OOOOO     O   O     O   O O     O  OO                         |");
		mvprintw(4, 0, "|                O         O   O     O     O     O   O                         |");
		mvprintw(5, 0, "|                O         OOOOO     O     O     OOOOO                         |");
		mvprintw(6, 0, "+==============================================================================+");
		mvprintw(7, 0, "Select difficulty and press enter: ");
		player_key_press = getch();
		switch(toupper(player_key_press)){
			case KEY_UP:
				menu_selection -= 1;
				break;
			case KEY_DOWN:
				menu_selection += 1;
				break;
			case KEY_RETURN:
				switch(menu_selection){
					case 0:
						GAME_ENDED = FALSE;
						difficulty = 80000; //Easy
						return;
					case 1:
						GAME_ENDED = FALSE;
						difficulty = 40000; //Normal
						return;
					case 2:
						GAME_ENDED = FALSE;
						difficulty = 20000; //Hard
						return;
					case 3:
						GAME_ENDED = FALSE;
						difficulty = 1; //Impossible
						return;
				}
				break;
			case 'Q':
				end_game();
				break;
			default:
				break;
		}
		if (menu_selection == -1){
			menu_selection = 3;
		}
		else if (menu_selection == 4){
			menu_selection = 0;
		}
		mvprintw(8, 0, "- Easy\n- Normal\n- Hard\n- Impossible");
		switch(menu_selection){
			case 0:
				mvaddch(8, 0, '>');
				break;
			case 1:
				mvaddch(9, 0, '>');
				break;
			case 2:
				mvaddch(10, 0, '>');
				break;
			case 3:
				mvaddch(11, 0, '>');
				break;
		}
		mvprintw(12, 0, "Licensed under the GPL-3.0, see included LICENSE file for more details.");
		refresh();
	}
	return;
}

void victory_for_player(){
	while (TRUE){
		clear();
		GAME_ENDED = TRUE;
		attron(COLOR_PAIR(4));
			printw("VICTORY FOR PLAYER!\nPress Enter to exit game.");
		attroff(COLOR_PAIR(4));
		player_key_press = getch();
		if (player_key_press == KEY_RETURN){
			end_game();
		}
	}
	return;
}

void victory_for_enemy(){
	while (TRUE){
		clear();
		GAME_ENDED = TRUE;
		attron(COLOR_PAIR(5));
			printw("DEFEAT FOR PLAYER!\nPress Enter to exit game.");
		attroff(COLOR_PAIR(5));
		player_key_press = getch();
		if (player_key_press == KEY_RETURN){
			end_game();
		}
	}
	return;
}

void end_game(){
	endwin();
	printf("Thank you for playing my game. =)");
	exit(0);
}